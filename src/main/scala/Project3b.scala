import java.nio.ByteBuffer
import java.security.MessageDigest
import java.util.concurrent.TimeoutException

import akka.actor._
import akka.util.Timeout
import me.tongfei.progressbar.ProgressBar

import scala.collection.mutable.ListBuffer
import akka.pattern.ask
import scala.concurrent.{Future, Await}
import scala.util.Random
import scala.concurrent.duration._
import scala.language.postfixOps

sealed trait ProjectThreeMessages

case class BossInit(numNodes: Int, numRequests: Int, bonus: Boolean) extends ProjectThreeMessages

case class ClosestPrecedingFingerReq(id: Long) extends ProjectThreeMessages
case class ClosestPrecedingFingerRsp(id: Long) extends ProjectThreeMessages

case class ClosestPrecedingFingerV2Req(id: Long) extends ProjectThreeMessages
case class ClosestPrecedingFingerV2Rsp(id: Long) extends ProjectThreeMessages

case class FindSuccessorReq(id: Long) extends ProjectThreeMessages
case class FindSuccessorRsp(id: Long) extends ProjectThreeMessages

case class FindSuccessorV2Req(id: Long) extends ProjectThreeMessages
case class FindSuccessorV2Rsp(id: Long) extends ProjectThreeMessages

case class FindPredecessorReq(id: Long) extends ProjectThreeMessages
case class FindPredecessorRsp(id: Long) extends ProjectThreeMessages

case class SubActorInit(parentId: Long, m: Int) extends ProjectThreeMessages
case class SubActorInitV2(parentId: Long, m: Int, successorId: Long,successorId1: Long, predecessorId: Long) extends ProjectThreeMessages

case object PleaseKillYourself extends ProjectThreeMessages

case object GetSuccessorReq extends ProjectThreeMessages
case class GetSuccessorRsp(successorId: Long) extends ProjectThreeMessages

case object GetPredecessorReq extends ProjectThreeMessages
case class GetPredecessorRsp(predecessorId: Long) extends ProjectThreeMessages

case class UpdatePredecessorReq(id: Long) extends ProjectThreeMessages
case class UpdatePredecessorRsp(result: Boolean) extends ProjectThreeMessages

case class InitFingerTableReq(nDash: Long) extends ProjectThreeMessages
case class InitFingerTableRsp(successorId: Long, predecessorId: Long, finger: List[(Long, Long)]) extends ProjectThreeMessages

case class JoinReq(nDash: Long) extends ProjectThreeMessages
case class JoinRsp(successorId: Long, predecessorId: Long, finger: List[(Long, Long)]) extends ProjectThreeMessages
case class JoinDone(result: Boolean) extends ProjectThreeMessages

case class JoinV2Req(nDash: Long) extends ProjectThreeMessages
case class JoinV2Rsp(successorId: Long) extends ProjectThreeMessages

case class NodeInit(thisNodesId: Long, m: Int) extends ProjectThreeMessages

case object UpdateOthersReq extends ProjectThreeMessages
case class UpdateOthersRsp(result: Boolean) extends ProjectThreeMessages

case class UpdateFingerTableReq(s: Long, i: Int) extends ProjectThreeMessages
case class UpdateFingerTableRsp(result: Boolean) extends ProjectThreeMessages

case class UpdateFingerTableReqToSa(s: Long, i: Int, finger: List[(Long, Long)], predecessorId: Long) extends ProjectThreeMessages
case class UpdateFingerTableRspFromSa(result: Boolean) extends ProjectThreeMessages

case class UpdateNtf(successorId: Long, predecessorId: Long, finger: List[(Long, Long)]) extends ProjectThreeMessages
case class UpdateFingerNtf(finger: List[(Long, Long)]) extends ProjectThreeMessages

case object AnybodyHome extends ProjectThreeMessages
case class YesImHome(id: Long) extends ProjectThreeMessages

case object NetworkBuildingDone extends ProjectThreeMessages
case object StartFindingKeys extends ProjectThreeMessages
case object RequestStats extends ProjectThreeMessages

case object StatsReq extends ProjectThreeMessages
case class StatsRsp(count: Int) extends ProjectThreeMessages

case object PrintAllData extends ProjectThreeMessages

case object StabilizeTimeout extends ProjectThreeMessages
case object StabilizeV2Req extends ProjectThreeMessages
case object StabilizeV2Done extends ProjectThreeMessages

case object CheckPTimeout extends ProjectThreeMessages
case object CheckPV2Req extends ProjectThreeMessages
case object CheckV2Done extends ProjectThreeMessages

case class UpdateSuccessorV2Ntf(successorId: Long) extends ProjectThreeMessages
case class UpdateSuccessorV2Ntf1(successorId1: Long) extends ProjectThreeMessages

case class NotifyV2Ntf(nDash: Long) extends ProjectThreeMessages

case object FixFingersTimeout extends ProjectThreeMessages
case class FixFingersV2Req(fingerStart: Long) extends ProjectThreeMessages
case class FixFingersV2Rsp(fingerNode: Long) extends ProjectThreeMessages

case object CheckCheck extends ProjectThreeMessages
case object KillaNode extends ProjectThreeMessages
case object CeaseAndDesist extends ProjectThreeMessages

class MyUtilities {
  def fallsIn(id: Long, start: Long, startStrict: Boolean, end: Long, endStrict: Boolean): Boolean = {
    var result: Boolean = false
    if (start < end) {
      if (startStrict == true && endStrict == true) {
        if (start < id && id < end) {
          result = true
        }
      } else if (startStrict == false && endStrict == false) {
        if (start <= id && id <= end) {
          result = true
        }
      } else if (startStrict == true && endStrict == false) {
        if (start < id && id <= end) {
          result = true
        }
      } else if (startStrict == false && endStrict == true) {
        if (start <= id && id < end) {
          result = true
        }
      }
    } else if (end < start) {
      result = true
      val newStart = end
      val newEnd = start
      val newStartStrict = !endStrict
      val newEndStrict = !startStrict
      if (newStartStrict == true && newEndStrict == true) {
        if (newStart < id && id < newEnd) {
          result = false
        }
      } else if (newStartStrict == false && newEndStrict == false) {
        if (newStart <= id && id <= newEnd) {
          result = false
        }
      } else if (newStartStrict == true && newEndStrict == false) {
        if (newStart < id && id <= newEnd) {
          result = false
        }
      } else if (newStartStrict == false && newEndStrict == true) {
        if (newStart <= id && id < newEnd) {
          result = false
        }
      }
    } else {
      if (id == start && (startStrict == false || endStrict == false)) {
        result = true
      } else {
        if (startStrict == false && endStrict == true) {
          result = true
        } else if (startStrict == true && endStrict == true) {
          result = true
        }

      }
    }

    return result
  }
}

class Boss extends Actor with ActorLogging {
  import context._

  var numberOfNodes: Int = 0
  var numberOfRequests: Int = 0
  var m: Int = 32
  val parallelSearchThreshold: Int = 1000

  var plainNodeIds: ListBuffer[Long] = new ListBuffer[Long]()
  var nodeIdsAndRefs: ListBuffer[(Long, ActorRef)] = new ListBuffer[(Long, ActorRef)]()


  var findingKeysRound: Int = 0
  var idResponses: ListBuffer[Long] = new ListBuffer[Long]()
  var statResponses: ListBuffer[Int] = new ListBuffer[Int]()
  var averageMessagesExchanged: ListBuffer[Float] = new ListBuffer[Float]()
  var randomKey: Long = 0

  implicit val someTimeout = Timeout(5 seconds)

  var pbKeys: ProgressBar = _


  def Sha256(s: String): Array[Byte] = {
    return MessageDigest.getInstance("SHA-256").digest(s.getBytes("UTF-8"))
  }

  def averageOfInts(list: ListBuffer[Int]): Float = list.foldLeft(0.toFloat)(_+_)/list.length.toFloat
  def averageOfFloats(list: ListBuffer[Float]): Float = list.foldLeft(0.toFloat)(_+_)/list.length

  def makeNewNodeId(): Long = {
    var plainNodeId = Random.nextInt(Int.MaxValue).toLong
//    var plainNodeId = Random.nextInt(1<<m).toLong
    while (plainNodeIds.contains(plainNodeId)) {
      plainNodeId = Random.nextInt(Int.MaxValue).toLong
//      plainNodeId = Random.nextInt(1<<m).toLong
    }
    plainNodeIds += plainNodeId
    return ByteBuffer.wrap(Sha256(plainNodeId.toString)).getLong() >>> (64-m)
//    return plainNodeId
  }

  def receive = {
    case BossInit(numNodes, numRequests, bonus) =>
      numberOfNodes = numNodes
      numberOfRequests = numRequests

      if (!bonus) {
        if (numberOfNodes > 10000)
          m = 40

        val pb = new ProgressBar("Building network", numberOfNodes, 50)
        pb.start()

        (0 until numberOfNodes).foreach(i => {
          val newNodeId = makeNewNodeId()
          val nodeIdAndRefPair = (newNodeId, context.system.actorOf(Props(new Node), newNodeId.toString))
          nodeIdAndRefPair._2 ! NodeInit(nodeIdAndRefPair._1, m)

          if (nodeIdsAndRefs.isEmpty) {
            val future: Future[JoinDone] = (nodeIdAndRefPair._2 ? JoinReq(-1.toLong)).mapTo[JoinDone]
            val joinDone = Await.result(future, someTimeout.duration)
            if (joinDone.result) {
              pb.step()
            }
          }
          else {
            val future: Future[JoinDone] = (nodeIdAndRefPair._2 ? JoinReq(nodeIdsAndRefs(Random.nextInt(nodeIdsAndRefs.length))._1)).mapTo[JoinDone]
            val joinDone = Await.result(future, someTimeout.duration)
            if (joinDone.result) {
              pb.step()
            }
          }

          nodeIdsAndRefs += nodeIdAndRefPair
        })

        pb.stop()

        self ! NetworkBuildingDone

      } else {

        m = 3
        numberOfNodes = 4

        (0 until numberOfNodes).foreach(i => {
          var newNodeId: Long = 0
          if (i==0)
            newNodeId = 0
          else if (i==1)
            newNodeId = 1
          else if (i==2)
            newNodeId = 3
          else if (i==3)
            newNodeId = 6

          val nodeIdAndRefPair = (newNodeId, context.system.actorOf(Props(new Node), newNodeId.toString))
          nodeIdAndRefPair._2 ! NodeInit(nodeIdAndRefPair._1, m)


          if (nodeIdsAndRefs.isEmpty) {
            val future: Future[JoinDone] = (nodeIdAndRefPair._2 ? JoinV2Req(-1.toLong)).mapTo[JoinDone]
            val joinDone = Await.result(future, someTimeout.duration)
            if (joinDone.result) {
              println(s"Joined $newNodeId")
            }
          } else {
            nodeIdAndRefPair._2 ! JoinV2Req(0.toLong)
          }

          nodeIdsAndRefs += nodeIdAndRefPair
        })

        val checkTimeout = Duration(10000, "millis")
        context.system.scheduler.scheduleOnce(checkTimeout, self, CheckCheck)
      }

    case JoinDone(result) =>
      println("Joined "+sender().path.name)
      val checkTimeout1 = Duration(100, "millis")
      context.system.scheduler.scheduleOnce(checkTimeout1,self, KillaNode)

    case CheckCheck =>
      val checkTimeout1 = Duration(100, "millis")
      //RJ
      //context.system.scheduler.scheduleOnce(checkTimeout1,self, KillaNode)
      
      (0 until numberOfNodes).foreach(i => {
        nodeIdsAndRefs(i)._2 ! CeaseAndDesist
        Thread.sleep(200)
      })
      context.system.shutdown()
           

    case KillaNode =>
      //TODOKilling node code
      (0 until numberOfNodes).foreach(i => {
        if (nodeIdsAndRefs(i)._1 == 1){
          println("Killing node"+nodeIdsAndRefs(i)._1)
          nodeIdsAndRefs(i)._2 ! PleaseKillYourself
        }
      })
      //Thread.sleep(1000)
      //(0 until numberOfNodes).foreach(i => println("node in list: "+nodeIdsAndRefs(i)._1))

    case NetworkBuildingDone =>
      if (numberOfNodes<parallelSearchThreshold)
        println ("Performing find key on all nodes at once")
      else
        println("Performing find key on one node at a time (not enough resources to do all together)")

      self ! RequestStats

    case StartFindingKeys =>
      findingKeysRound += 1

      randomKey = makeNewNodeId()

      if (numberOfNodes < parallelSearchThreshold) {
        nodeIdsAndRefs.foreach(aPair => {
          aPair._2 ! FindSuccessorReq(randomKey)
        })
      } else {

        nodeIdsAndRefs.foreach(aPair => {
          val future: Future[FindSuccessorRsp] = (aPair._2 ? FindSuccessorReq(randomKey)).mapTo[FindSuccessorRsp]
          val findDone = Await.result(future, someTimeout.duration)
          idResponses += findDone.id
        })
        if (idResponses.forall(response => {response==idResponses(0)})) {
        } else {
          println("Some error in finding keys")
        }
        idResponses.clear()
        self ! RequestStats
      }

    case FindSuccessorRsp(idIn) =>
      idResponses += idIn

      if (idResponses.length == numberOfNodes) {
        if (idResponses.forall(response => {response==idResponses(0)})) {
          // print key, id mapping if needed here
        } else {
          println("Some error in finding keys")
        }
        idResponses.clear()
        self ! RequestStats
      }

    case RequestStats =>
      nodeIdsAndRefs.foreach(aPair => {
        aPair._2 ! StatsReq
      })

    case StatsRsp(countIn) =>
      statResponses += countIn

      if (statResponses.length == numberOfNodes) {
        if (findingKeysRound != 0) {
          var averageMsgCount: Float = averageOfInts(statResponses)
          averageMessagesExchanged += averageMsgCount
          pbKeys.setExtraMessage("Running avg. "+averageOfFloats(averageMessagesExchanged).toString)
        } else {
          pbKeys = new ProgressBar("Finding keys    ", numberOfRequests, 50)
          pbKeys.start()
        }

        statResponses.clear()
        if (findingKeysRound == numberOfRequests) {
          pbKeys.stop()
          println("Avg. number of messages exchanged "+averageOfFloats(averageMessagesExchanged).toString)
          println("That's all folks!")
          context.system.shutdown()
        } else {
          pbKeys.step()
          self ! StartFindingKeys
        }
      }
  }
}

class JoinV2SubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  var mySuccessorId: Long = _
  var mySuccessorId1: Long = _
  var myPredecessorId: Long = _
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInitV2(parentId, m, successorId, successorId1, predecessorId) =>
      myParentId = parentId
      systemM = m
      mySuccessorId = successorId
      mySuccessorId1 = successorId1
      myPredecessorId = predecessorId

    case PleaseKillYourself =>
      context.stop(self)

    case JoinV2Req(nDash) =>
      // find my successor using nDash -> nDash ? FindSuccessorV2Req(myParentId) -> FindSuccessorV2Rsp(id) -> JoinV2Rsp(successorId)-> EOD

      val future: Future[FindSuccessorV2Rsp] = (context.system.actorSelection("/user/"+nDash.toString) ? FindSuccessorV2Req(myParentId)).mapTo[FindSuccessorV2Rsp]
      val findSuccessorV2Rsp = Await.result(future, someTimeout.duration)

      context.system.actorSelection("/user/"+myParentId.toString) ! JoinV2Rsp(findSuccessorV2Rsp.id)
      /*RJ
      val future2: Future[FindSuccessorV2Rsp] = (context.system.actorSelection("/user/"+findSuccessorV2Rsp.id.toString) ? FindSuccessorV2Req(myParentId)).mapTo[FindSuccessorV2Rsp]
      val findSuccessorV2Rsp2 = Await.result(future2, someTimeout.duration)

      //context.system.actorSelection("/user/"+myParentId.toString) ! JoinV2Rsp(findSuccessorV2Rsp.id)
      RJ*/
  }
}

class FindSuccessorV2SubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  var mySuccessorId: Long = _
  var myPredecessorId: Long = _
  var myUtils: MyUtilities = new MyUtilities()

  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInitV2(parentId, m, successorId, successorId1, predecessorId) =>
      myParentId = parentId
      systemM = m
      mySuccessorId = successorId
      myPredecessorId = predecessorId

    case PleaseKillYourself =>
      context.stop(self)

    case FindSuccessorV2Req(idIn) =>
      if (myUtils.fallsIn(idIn, myParentId, true, mySuccessorId, false)) {
        // if idIn belongs to (n, successor], then return successor
        context.system.actorSelection("/user/"+myParentId.toString) ! FindSuccessorV2Rsp(mySuccessorId)
      } else {
        // find myParentId ? ClosestPrecedingNodeV2Req(idIn) -> assign this to nDash -> do nDash ? FindSuccessorV2Req(idIn) -> return this successor
        val future: Future[ClosestPrecedingFingerV2Rsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? ClosestPrecedingFingerV2Req(idIn)).mapTo[ClosestPrecedingFingerV2Rsp]
        val closestPrecedingFingerV2Rsp = Await.result(future, someTimeout.duration)

        val nDash = closestPrecedingFingerV2Rsp.id

        if (nDash == myParentId) {
          context.system.actorSelection("/user/"+myParentId.toString) ! FindSuccessorV2Rsp(nDash)
        } else {
          val futureFsv2r: Future[FindSuccessorV2Rsp] = (context.system.actorSelection("/user/"+nDash.toString) ? FindSuccessorV2Req(idIn)).mapTo[FindSuccessorV2Rsp]
          val findSuccessorV2Rsp = Await.result(futureFsv2r, someTimeout.duration)

          context.system.actorSelection("/user/"+myParentId.toString) ! FindSuccessorV2Rsp(findSuccessorV2Rsp.id)
        }
      }
  }
}

class StabilizeV2SubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  var mySuccessorId: Long = _
  var mySuccessorId1: Long = _
  var myPredecessorId: Long = _
  implicit val someTimeout = Timeout(5 seconds)
  val myUtils = new MyUtilities()

  def receive = {
    case SubActorInitV2(parentId, m, successorId,successorId1, predecessorId) =>
      myParentId = parentId
      systemM = m
      mySuccessorId = successorId
      mySuccessorId1 = successorId1
      myPredecessorId = predecessorId

    case PleaseKillYourself =>
      context.stop(self)

    case StabilizeV2Req =>
      // get the predecessor from successor
      // mySuccessorId ? GetPredecessorReq -> assign to x
      //RJ Need an actor reference here if it doesn't exist get the predecessor and successor using successorId1//
      /*
      (0 until numberOfNodes).foreach(i => {
        println("stNode"+nodeIdsAndRefs(i)._1)
      })
      
      for (i <- 0 until systemM) {
        val twoPowerI = 1<<i
        var n = myParentId
        if (n < twoPowerI) {
          n = n + (1<<systemM)
        }
        val nMinusTwoPowerI = n - twoPowerI
      */
        println("In Stabilize request"+myParentId)
        var anybodyHomeException: Boolean = false
        val futureAh: Future[YesImHome] = (context.system.actorSelection("/user/"+myParentId.toString) ? AnybodyHome).mapTo[YesImHome]
        try {
          Await.result(futureAh, 100.milliseconds)
        } catch {
          case e:TimeoutException =>
            anybodyHomeException = true
        }

        //var p: Long = -1
        if (anybodyHomeException) {
          println("Node not present"+myParentId)
        }

      val future: Future[GetPredecessorRsp] = (context.system.actorSelection("/user/"+mySuccessorId.toString) ? GetPredecessorReq).mapTo[GetPredecessorRsp]
      val getPredecessorRsp = Await.result(future, someTimeout.duration)

      val x = getPredecessorRsp.predecessorId
      //println("id: "+myParentId+" predecessor in stabilize"+x)

      // get the successor from successor
      // mySuccessorId ? GetSuccessorReq -> assign to y

      val future2: Future[GetSuccessorRsp] = (context.system.actorSelection("/user/"+mySuccessorId.toString) ? GetSuccessorReq).mapTo[GetSuccessorRsp]
      val getSuccessorRsp = Await.result(future2, someTimeout.duration)

      val y = getSuccessorRsp.successorId
      //println("id: "+myParentId+" successor1 in stabilize"+mySuccessorId)
      //println("id: "+myParentId+" successor2 in stabilize"+y)

      // check x not equal to -1
      if (x != -1) {
        // if x belongs to (myParentId, mySuccessorId) -> update my successor to x -> myParentId ! UpdateSuccessorV2Ntf
        if (myUtils.fallsIn(x, myParentId, true, mySuccessorId, true)) {
          mySuccessorId = x
          context.system.actorSelection("/user/"+myParentId.toString) ! UpdateSuccessorV2Ntf(mySuccessorId)
        }
        if (y != -1) {
        // if x belongs to (myParentId, mySuccessorId) -> update my successor to x -> myParentId ! UpdateSuccessorV2Ntf
          //if (myUtils.fallsIn(x, myParentId, true, mySuccessorId, true)) {
          mySuccessorId1 = y
          context.system.actorSelection("/user/"+myParentId.toString) ! UpdateSuccessorV2Ntf1(mySuccessorId1)
          //}
        } 
      }

      //in any case, i.e. on my current successor do a notify
      context.system.actorSelection("/user/"+mySuccessorId.toString) ! NotifyV2Ntf(myParentId)
      context.system.actorSelection("/user/"+myParentId.toString) ! StabilizeV2Done

    case CheckPV2Req =>
      //check for predecssor valid or not else make it nil(-1)
      val future: Future[GetPredecessorRsp] = (context.system.actorSelection("/user/"+mySuccessorId.toString) ? GetPredecessorReq).mapTo[GetPredecessorRsp]
      val getPredecessorRsp = Await.result(future, someTimeout.duration)
      //TODO if the fetch fails then node has died.  Make my predecessor nil 
      val p = getPredecessorRsp.predecessorId
  }
}

class FixFingersV2SubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  var mySuccessorId: Long = _
  var myPredecessorId: Long = _
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInitV2(parentId, m, successorId,successorId1, predecessorId) =>
      myParentId = parentId
      systemM = m
      mySuccessorId = successorId
      myPredecessorId = predecessorId

    case PleaseKillYourself =>
      context.stop(self)

    case FixFingersV2Req(fingerStart) =>
      val future: Future[FindSuccessorV2Rsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? FindSuccessorV2Req(fingerStart)).mapTo[FindSuccessorV2Rsp]
      val findSuccessorV2Rsp = Await.result(future, someTimeout.duration)

      context.system.actorSelection("/user/"+myParentId.toString) ! FixFingersV2Rsp(findSuccessorV2Rsp.id)
  }
}

class JoinSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId
      systemM = m

    case PleaseKillYourself =>
      context.stop(self)

    case JoinReq(nDash) =>
      var successorId: Long = -1
      var predecessorId: Long = -1
      var finger: List[(Long, Long)] = null

      if (nDash != -1) {
        val future: Future[InitFingerTableRsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? InitFingerTableReq(nDash)).mapTo[InitFingerTableRsp]
        val initFingerTableRsp = Await.result(future, someTimeout.duration)

        successorId = initFingerTableRsp.successorId
        predecessorId = initFingerTableRsp.predecessorId
        finger = initFingerTableRsp.finger

        context.system.actorSelection("/user/"+myParentId.toString) ! UpdateNtf(successorId, predecessorId, finger)

        val futureUor: Future[UpdateOthersRsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? UpdateOthersReq).mapTo[UpdateOthersRsp]
        val updateOthersRsp = Await.result(futureUor, someTimeout.duration)
        if (!updateOthersRsp.result) {
        }

        context.system.actorSelection("/user/"+myParentId.toString) ! JoinRsp(successorId, predecessorId, finger)
      } else {
        successorId = myParentId
        predecessorId = myParentId
        val fingerArray: Array[(Long, Long)] = new Array[(Long, Long)](systemM)
        (0 until systemM).foreach(k => {
          fingerArray(k) = ((myParentId + (1<<k))%(1<<systemM), myParentId)
        })
        finger = fingerArray.toList

        context.system.actorSelection("/user/"+myParentId.toString) ! UpdateNtf(successorId, predecessorId, finger)
        context.system.actorSelection("/user/"+myParentId.toString) ! JoinRsp(successorId, predecessorId, finger)
      }
  }
}

class FindSuccessorSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId

    case PleaseKillYourself =>
      context.stop(self)

    case FindSuccessorReq(idIn) =>
      val future: Future[FindPredecessorRsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? FindPredecessorReq(idIn)).mapTo[FindPredecessorRsp]
      val findPredecessorRsp = Await.result(future, someTimeout.duration)
      val nDash = findPredecessorRsp.id

      val futureGsr: Future[GetSuccessorRsp] = (context.system.actorSelection("/user/"+nDash.toString) ? GetSuccessorReq).mapTo[GetSuccessorRsp]
      val getSuccessorRsp = Await.result(futureGsr, someTimeout.duration)
      val nDashDotSuccessor = getSuccessorRsp.successorId

      context.system.actorSelection("/user/"+myParentId.toString) ! FindSuccessorRsp(nDashDotSuccessor)
  }
}

class FindPredecessorSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  val myUtils = new MyUtilities()
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId

    case PleaseKillYourself =>
      context.stop(self)

    case FindPredecessorReq(idIn) =>
      val future: Future[GetSuccessorRsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? GetSuccessorReq).mapTo[GetSuccessorRsp]
      val getSuccessorRsp = Await.result(future, someTimeout.duration)

      var nDash = myParentId
      var nDashDotSuccessor: Long = getSuccessorRsp.successorId

      var notBelongsToCondition: Boolean = !myUtils.fallsIn(idIn, nDash, true, nDashDotSuccessor, false)

      while (notBelongsToCondition) {
        val futureCpfr : Future[ClosestPrecedingFingerRsp] = (context.system.actorSelection("/user/"+nDash.toString) ? ClosestPrecedingFingerReq(idIn)).mapTo[ClosestPrecedingFingerRsp]
        val closestPrecedingFingerRsp = Await.result(futureCpfr, someTimeout.duration)

        if (nDash == closestPrecedingFingerRsp.id) {
          notBelongsToCondition = false
        } else {
          nDash = closestPrecedingFingerRsp.id
          val futureGsr: Future[GetSuccessorRsp] = (context.system.actorSelection("/user/"+nDash.toString) ? GetSuccessorReq).mapTo[GetSuccessorRsp]
          val getSuccessorRsp = Await.result(futureGsr, someTimeout.duration)
          nDashDotSuccessor = getSuccessorRsp.successorId
        }
      }

      context.system.actorSelection("/user/"+myParentId.toString) ! FindPredecessorRsp(nDash)
  }
}

class InitFingerTableSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  implicit val someTimeout = Timeout(5 seconds)
  val myUtils = new MyUtilities()

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId
      systemM = m

    case PleaseKillYourself =>
      context.stop(self)

    case InitFingerTableReq(nDash) =>
      val finger: Array[(Long, Long)] = new Array[(Long, Long)](systemM)
      (0 until systemM).foreach(k => {
        finger(k) = ((myParentId + (1.toLong<<k))%(1.toLong<<systemM), -1.toLong)
      })

      val future: Future[FindSuccessorRsp] = (context.system.actorSelection("/user/"+nDash.toString) ? FindSuccessorReq(finger(0)._1)).mapTo[FindSuccessorRsp]
      val findSuccessorRsp = Await.result(future, someTimeout.duration)
      finger(0) = (finger(0)._1, findSuccessorRsp.id)
      val newSuccessor = finger(0)._2

      val futureGpr: Future[GetPredecessorRsp] = (context.system.actorSelection("/user/"+newSuccessor.toString) ? GetPredecessorReq).mapTo[GetPredecessorRsp]
      val getPredecessorRsp = Await.result(futureGpr, someTimeout.duration)
      val newPredecessor = getPredecessorRsp.predecessorId

      val futureUpr: Future[UpdatePredecessorRsp] = (context.system.actorSelection("/user/"+newSuccessor.toString) ? UpdatePredecessorReq(myParentId)).mapTo[UpdatePredecessorRsp]
      val updatePredecessorRsp = Await.result(futureUpr, someTimeout.duration)
      if(!updatePredecessorRsp.result) {
      }

      val n = myParentId

      for (i <- 0 to systemM-2) {
        if (myUtils.fallsIn(finger(i+1)._1, myParentId, false, finger(i)._2, true)) {
          finger(i+1) = (finger(i+1)._1, finger(i)._2)
        }
        else {
          val future: Future[FindSuccessorRsp] = (context.system.actorSelection("/user/"+nDash.toString) ? FindSuccessorReq(finger(i+1)._1)).mapTo[FindSuccessorRsp]
          val findSuccessorRsp = Await.result(future, someTimeout.duration)
          finger(i+1) = (finger(i+1)._1, findSuccessorRsp.id)
        }
      }

      context.system.actorSelection("/user/"+myParentId.toString) ! InitFingerTableRsp(newSuccessor, newPredecessor, finger.toList)
  }
}

class UpdateOthersSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId
      systemM = m

    case PleaseKillYourself =>
      context.stop(self)

    case UpdateOthersReq =>
      for (i <- 0 until systemM) {
        val twoPowerI = 1<<i
        var n = myParentId
        if (n < twoPowerI) {
          n = n + (1<<systemM)
        }
        val nMinusTwoPowerI = n - twoPowerI

        var anybodyHomeException: Boolean = false
        val futureAh: Future[YesImHome] = (context.system.actorSelection("/user/"+nMinusTwoPowerI.toString) ? AnybodyHome).mapTo[YesImHome]
        try {
          Await.result(futureAh, 1.milliseconds)
        } catch {
          case e:TimeoutException =>
            anybodyHomeException = true
        }

        var p: Long = -1
        if (anybodyHomeException) {
          val future: Future[FindPredecessorRsp] = (context.system.actorSelection("/user/"+myParentId.toString) ? FindPredecessorReq(nMinusTwoPowerI)).mapTo[FindPredecessorRsp]
          val findPredecessorRsp = Await.result(future, someTimeout.duration)
          p = findPredecessorRsp.id
        } else {
          p = nMinusTwoPowerI
        }

        val futureUftr: Future[UpdateFingerTableRsp] = (context.system.actorSelection("/user/"+p.toString) ? UpdateFingerTableReq(myParentId, i)).mapTo[UpdateFingerTableRsp]
        val updateFingerTableRsp = Await.result(futureUftr, someTimeout.duration)
        if (!updateFingerTableRsp.result) {
        }
      }

      context.system.actorSelection("/user/"+myParentId.toString) ! UpdateOthersRsp(true)
  }
}

class UpdateFingerTableSubActor extends Actor with ActorLogging {
  var systemM: Int = 0
  var myParentId: Long = _
  val myUtils = new MyUtilities()
  implicit val someTimeout = Timeout(5 seconds)

  def receive = {
    case SubActorInit(parentId, m) =>
      myParentId = parentId
      systemM = m

    case PleaseKillYourself =>
      context.stop(self)

    case UpdateFingerTableReqToSa(sIn, iIn, fingerIn, predecessorId) =>
      val finger = fingerIn.toArray
      if (myUtils.fallsIn(sIn, myParentId, false, finger(iIn)._2, true)) {
        finger(iIn) = (finger(iIn)._1, sIn)
        context.system.actorSelection("/user/"+myParentId.toString) ! UpdateFingerNtf(finger.toList)

        val future: Future[UpdateFingerTableRsp] = (context.system.actorSelection("/user/"+predecessorId.toString) ? UpdateFingerTableReq(sIn, iIn)).mapTo[UpdateFingerTableRsp]
        val updateFingerTableRsp = Await.result(future, someTimeout.duration)
        if (!updateFingerTableRsp.result) {
        }
      }

      context.system.actorSelection("/user/"+myParentId.toString) ! UpdateFingerTableRspFromSa(true)
  }
}

class Node extends Actor with ActorLogging {
  import context._

  var systemM: Int = 0
  var myId: Long = 0
  var mySuccessorsId: Long = 0
  var mySuccessorsId1: Long = 0
  //var mySuccessorsId: List[Int]
  var myPredecessorsId: Long = 0
  var myFinger: List[(Long, Long)] = _
  // _1 = message received from ref, _2 = sub Actor to whom it was forwarded
  var forwardingMap : ListBuffer[(ActorRef, ActorRef)] = new ListBuffer[(ActorRef, ActorRef)]()
  var mySubActorCount: Int = 0
  val myUtils = new MyUtilities()
  var joinInProgress: Boolean = false
  var msgInCount: Int = 0
  var msgOutCount: Int = 0
  var kill_flag: Int = 10
  val stabilizeDuration = Duration(500, "millis")
  var stabilizeCancellable: Cancellable = _
  var stabilizeV2SubActorRef: ActorRef = _

  val checkPredecessorDuration = Duration(10, "millis")
  var checkPredecessorCancellable: Cancellable = _
  var checkPredecessorV2SubActorRef: ActorRef = _

  val fixFingersDuration = Duration(10, "millis")
  var fixFingersCancellable: Cancellable = _
  var fixFingersV2SubActorRef: ActorRef = _
  var fixFingersNext: Int = 0

  def Closest_preceding_finger(idIn: Long): Long = {
    (systemM-1 to 0 by -1).foreach(i => {
      if (myUtils.fallsIn(myFinger(i)._2, myId, true, idIn, true)) {
        return myFinger(i)._2
      }
    })
    return myId
  }

  def receive = {
    case NodeInit(thisNodesId, m) =>
      myId = thisNodesId
      systemM = m
      mySuccessorsId = -1
      mySuccessorsId1 = -1
      myPredecessorsId = -1

    case JoinReq(nDash) =>
      joinInProgress = true
      mySubActorCount += 1
      val joinReqSubActorRef = context.system.actorOf(Props(new JoinSubActor), name = myId.toString + "_J_" + mySubActorCount.toString)
      val forwardingPair = (sender, joinReqSubActorRef)
      forwardingMap += forwardingPair
      joinReqSubActorRef ! SubActorInit(myId, systemM)
      joinReqSubActorRef ! JoinReq(nDash)

    case JoinRsp(successorId, predecessorId, finger) =>
      joinInProgress = false
      mySuccessorsId = successorId
      myPredecessorsId = predecessorId
      myFinger = finger
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(x => {x._2 == sender}))
      forwardingMapPair._1 ! JoinDone(true)
      forwardingMapPair._2 ! PleaseKillYourself

    case JoinV2Req(nDash) =>
      // init finger table with default value
      val fingerArray: Array[(Long, Long)] = new Array[(Long, Long)](systemM)
      (0 until systemM).foreach(k => {
        fingerArray(k) = ((myId + (1<<k))%(1<<systemM), myId)
      })
      myFinger = fingerArray.toList

      if (nDash == -1) {
        // create case
        myPredecessorsId = -1
        mySuccessorsId = myId
        stabilizeCancellable = context.system.scheduler.scheduleOnce(Duration(2000, "millis"), self, StabilizeTimeout)
        fixFingersCancellable = context.system.scheduler.scheduleOnce(Duration(3000, "millis"), self, FixFingersTimeout)
        sender ! JoinDone(true)
      } else {
        // init finger table
        joinInProgress = true
        mySubActorCount += 1
        val joinV2ReqSubActorRef = context.system.actorOf(Props(new JoinV2SubActor), myId.toString + "_JV2_" + mySubActorCount.toString)
        val forwardingPair = (sender, joinV2ReqSubActorRef)
        forwardingMap += forwardingPair
        joinV2ReqSubActorRef ! SubActorInitV2(myId, systemM, mySuccessorsId, mySuccessorsId1, myPredecessorsId)
        joinV2ReqSubActorRef ! JoinV2Req(nDash)
      }

    case JoinV2Rsp(successorIdIn) =>
      mySuccessorsId = successorIdIn

      val fingersArray = myFinger.toArray
      fingersArray(0) = (fingersArray(0)._1, successorIdIn)
      myFinger = fingersArray.toList

      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(x => {x._2 == sender}))
      forwardingMapPair._1 ! JoinDone(true)
      forwardingMapPair._2 ! PleaseKillYourself

      // start stabilize timeout and fix fingers timeout
      stabilizeCancellable = context.system.scheduler.scheduleOnce(stabilizeDuration, self, StabilizeTimeout)
      fixFingersCancellable = context.system.scheduler.scheduleOnce(fixFingersDuration, self, FixFingersTimeout)

    case FindSuccessorV2Req(idIn) =>
      mySubActorCount += 1
      val findSuccessorV2SubActorRef = context.system.actorOf(Props(new FindSuccessorV2SubActor), myId.toString + "_FSV2_" + mySubActorCount.toString)
      val forwardingPair = (sender, findSuccessorV2SubActorRef)
      forwardingMap += forwardingPair
      findSuccessorV2SubActorRef ! SubActorInitV2(myId, systemM, mySuccessorsId,mySuccessorsId1, myPredecessorsId)
      findSuccessorV2SubActorRef ! FindSuccessorV2Req(idIn)

    case FindSuccessorV2Rsp(idIn) =>
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(x => {x._2 == sender}))
      forwardingMapPair._1 ! FindSuccessorV2Rsp(idIn)
      forwardingMapPair._2 ! PleaseKillYourself

    case StabilizeTimeout =>
      mySubActorCount += 1
      stabilizeV2SubActorRef = context.system.actorOf(Props(new StabilizeV2SubActor), myId.toString + "_SV2_" + mySubActorCount.toString)
      //println("At node#"+myId+"in StabilizeTimeout")
      stabilizeV2SubActorRef ! SubActorInitV2(myId, systemM, mySuccessorsId,mySuccessorsId1, myPredecessorsId)
      stabilizeV2SubActorRef ! StabilizeV2Req

    case UpdateSuccessorV2Ntf(successorId) =>
      if (successorId != mySuccessorsId) {
        println("At node#"+myId.toString+" successor updated "+mySuccessorsId.toString+" -> "+successorId.toString)
        mySuccessorsId = successorId

        val fingersArray = myFinger.toArray
        fingersArray(0) = (fingersArray(0)._1, successorId)
        myFinger = fingersArray.toList
      }

    case UpdateSuccessorV2Ntf1(successorId1) =>
      if (successorId1 != mySuccessorsId) {
        println("At node#"+myId.toString+" successor updated "+mySuccessorsId.toString+" -> "+successorId1.toString)
        mySuccessorsId1 = successorId1
        /*
        val fingersArray = myFinger.toArray
        fingersArray(0) = (fingersArray(0)._1, successorId)
        myFinger = fingersArray.toList
        */
      }

    case StabilizeV2Done =>
      stabilizeV2SubActorRef ! PleaseKillYourself 
      //RJ
      //forwardingMapPair._1 ! PleaseKillYourself
      stabilizeCancellable = context.system.scheduler.scheduleOnce(stabilizeDuration, self, StabilizeTimeout)
      /*
      kill_flag += 1
      println("kill flag"+kill_flag)
      if((myId==1) && (kill_flag > 15)){
        println("killing node1")
        context.system.actorSelection("/user/"+myId.toString) ! PleaseKillYourself
      }*/
      

    case FixFingersTimeout =>
      mySubActorCount += 1
      fixFingersV2SubActorRef = context.system.actorOf(Props(new FixFingersV2SubActor), myId.toString + "_FFV2_" + mySubActorCount.toString)
      fixFingersV2SubActorRef ! SubActorInitV2(myId, systemM, mySuccessorsId,mySuccessorsId1, myPredecessorsId)
      fixFingersV2SubActorRef ! FixFingersV2Req(myFinger(fixFingersNext)._1)

    case FixFingersV2Rsp(fingerNodeIn) =>
      fixFingersV2SubActorRef ! PleaseKillYourself
      fixFingersCancellable = context.system.scheduler.scheduleOnce(fixFingersDuration, self, FixFingersTimeout)

      if (fingerNodeIn != myFinger(fixFingersNext)._2) {
        println("At node#"+myId.toString+" finger("+fixFingersNext.toString+") updated "+myFinger(fixFingersNext)._2.toString+" -> "+fingerNodeIn.toString)
        val fingersArray = myFinger.toArray
        fingersArray(fixFingersNext) = (fingersArray(fixFingersNext)._1, fingerNodeIn)
        myFinger = fingersArray.toList
      }
      fixFingersNext += 1
      if (fixFingersNext == systemM)
        fixFingersNext = 0

    case NotifyV2Ntf(nDash) =>
      if (myPredecessorsId == -1 || myUtils.fallsIn(nDash, myPredecessorsId, true, myId, true))
        myPredecessorsId = nDash

    case InitFingerTableReq(nDash) =>
      mySubActorCount += 1
      val initFingerTableSubActorRef = context.system.actorOf(Props(new InitFingerTableSubActor), name = myId.toString + "_IFT_" + mySubActorCount.toString)
      val forwardingPair = (sender, initFingerTableSubActorRef)
      forwardingMap += forwardingPair
      initFingerTableSubActorRef ! SubActorInit(myId, systemM)
      initFingerTableSubActorRef ! InitFingerTableReq(nDash)

    case InitFingerTableRsp(successorId, predecessorId, finger) =>
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(x => {x._2 == sender}))
      forwardingMapPair._1 ! InitFingerTableRsp(successorId, predecessorId, finger)
      forwardingMapPair._2 ! PleaseKillYourself

    case FindSuccessorReq(idIn) =>
      msgInCount += 1
      if (idIn == myId) {
        sender ! FindSuccessorRsp(myId)
      } else {
        mySubActorCount += 1
        val findSuccessorSubActorRef = context.system.actorOf(Props(new FindSuccessorSubActor), name = myId.toString + "_FS_" + mySubActorCount.toString)
        val forwardingPair = (sender, findSuccessorSubActorRef)
        forwardingMap += forwardingPair
        findSuccessorSubActorRef ! SubActorInit(myId, systemM)
        findSuccessorSubActorRef ! FindSuccessorReq(idIn)
      }

    case FindSuccessorRsp(idIn) =>
      msgOutCount += 1
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(x => {x._2 == sender}))
      forwardingMapPair._1 ! FindSuccessorRsp(idIn)
      forwardingMapPair._2 ! PleaseKillYourself

    case FindPredecessorReq(idIn) =>
      msgInCount += 1
      if (idIn == myId) {
        sender ! FindPredecessorRsp(myPredecessorsId)
      } else {
        mySubActorCount += 1
        val findPredecessorSubActorRef = context.system.actorOf(Props(new FindPredecessorSubActor), name = myId.toString + "_FP_Inst_" + mySubActorCount.toString)
        val forwardingMapPair = (sender, findPredecessorSubActorRef)
        forwardingMap += forwardingMapPair
        findPredecessorSubActorRef ! SubActorInit(myId, systemM)
        findPredecessorSubActorRef ! FindPredecessorReq(idIn)
      }

    case FindPredecessorRsp(idIn) =>
      msgInCount += 1
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(forwardingMapPair => {forwardingMapPair._2 == sender}))
      forwardingMapPair._1 ! FindPredecessorRsp(idIn)
      forwardingMapPair._2 ! PleaseKillYourself

    case ClosestPrecedingFingerReq(idIn) =>
      msgInCount += 1
      msgOutCount += 1
      val idOut: Long = Closest_preceding_finger(idIn)
      sender ! ClosestPrecedingFingerRsp(idOut)

    case ClosestPrecedingFingerV2Req(idIn) =>
      val idOut: Long = Closest_preceding_finger(idIn)
      sender ! ClosestPrecedingFingerV2Rsp(idOut)

    case GetSuccessorReq =>
      sender ! GetSuccessorRsp(mySuccessorsId)

    case GetPredecessorReq =>
      sender ! GetPredecessorRsp(myPredecessorsId)

    case UpdatePredecessorReq(idIn) =>
      myPredecessorsId = idIn
      sender ! UpdatePredecessorRsp(true)

    case UpdateOthersReq =>
      mySubActorCount += 1
      val updateOthersSubActorRef = context.system.actorOf(Props(new UpdateOthersSubActor), name = myId.toString + "_UA_Inst_" + mySubActorCount.toString)
      val forwardingMapPair = (sender, updateOthersSubActorRef)
      forwardingMap += forwardingMapPair
      updateOthersSubActorRef ! SubActorInit(myId, systemM)
      updateOthersSubActorRef ! UpdateOthersReq

    case UpdateOthersRsp(result) =>
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(forwardingMapPair => {forwardingMapPair._2 == sender}))
      forwardingMapPair._1 ! UpdateOthersRsp(result)
      forwardingMapPair._2 ! PleaseKillYourself

    case UpdateFingerTableReq(s, i) =>
      if (joinInProgress) {
        sender ! UpdateFingerTableRsp(true)
      } else {
        mySubActorCount += 1
        val updateFingerTableSubActorRef = context.system.actorOf(Props(new UpdateFingerTableSubActor), name = myId.toString + "_UFT_Inst_" + mySubActorCount.toString)
        val forwardingMapPair = (sender, updateFingerTableSubActorRef)
        forwardingMap += forwardingMapPair
        updateFingerTableSubActorRef ! SubActorInit(myId, systemM)
        updateFingerTableSubActorRef ! UpdateFingerTableReqToSa(s, i, myFinger, myPredecessorsId)
      }

    case UpdateFingerTableRspFromSa(result) =>
      val forwardingMapPair = forwardingMap.remove(forwardingMap.indexWhere(forwardingMapPair => {forwardingMapPair._2 == sender}))
      forwardingMapPair._1 ! UpdateFingerTableRsp(result)
      forwardingMapPair._2 ! PleaseKillYourself

    case UpdateNtf(successorId, predecessorId, finger) =>
      mySuccessorsId = successorId
      myPredecessorsId = predecessorId
      myFinger = finger

    case UpdateFingerNtf(fingerIn) =>
      myFinger = fingerIn
      mySuccessorsId = myFinger(0)._2

    case AnybodyHome =>
      sender ! YesImHome(myId)

    case StatsReq =>
      sender ! StatsRsp(msgInCount)
      msgInCount = 0
      msgOutCount = 0

    case PrintAllData =>
//      log.info("predecessor "+myPredecessorsId.toString)
//      log.info("successor "+mySuccessorsId.toString)
//      myFinger.foreach(f => {
//        log.info("start "+f._1+" node "+f._2)
//      })

    case CeaseAndDesist =>
//      stabilizeCancellable.cancel()
//      fixFingersCancellable.cancel()
      println("At node#"+myId.toString)
      println("predecessor "+myPredecessorsId.toString)
      println("successor1 "+mySuccessorsId.toString)
      println("successor2 "+mySuccessorsId1.toString)
      myFinger.foreach(f => {
        println("start "+f._1+" node "+f._2)
      })
      println("~ ~ ~")

    case PleaseKillYourself =>
        context.stop(self)
  }
}

object Project3 {
  def main (args: Array[String]) {
    var wrongUsage: Boolean = false
    var numNodes: Int = 0
    var numRequests: Int = 0
    var bonus: Boolean = false

    if (args.length == 2) {
      numNodes = args(0).toInt
      numRequests = args(1).toInt
    } else if (args.length == 3) {
      numNodes = args(0).toInt
      numRequests = args(1).toInt
      bonus = args(2).equals("bonus")
    }

    else wrongUsage = true

    if(wrongUsage) println("Usage: numNodes numRequests")
    else ActorSystem("Project3").actorOf(Props(new Boss), "Boss") ! BossInit(numNodes, numRequests, bonus)
  }
}
