name := "dos-project-3-chord"

version := "1.0"

scalaVersion := "2.11.7"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.13"

libraryDependencies += "me.tongfei" %% "progressbar" % "0.3.2"